﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Company
{
    public sealed class BookKeeping : IBookKeeping
    {
        public void CalculateSalary(Worker worker)
        {
            var workerHaveSubWorkers = !(worker is Employee) || worker is IBoss && ((IBoss)worker).SubWorkers.Any();

            if (workerHaveSubWorkers)
            {
               CalculateSalaryForWorkers(((IBoss)worker).SubWorkers);
            }

            worker.Salary_InRubles = worker.Rate_InRuble;

            foreach (var bonusRule in worker.BonusCalculationRules)
            {
                worker.Salary_InRubles += bonusRule.CalculateBonus(worker,);
            }
        }

        public void CalculateSalaryForWorkers(List<Worker> workers)
        {
            foreach (var w in workers)
            {
                CalculateSalary(w);
            }
        }

        public decimal CalculateYearBonusSalary(Worker worker, DateTime date)
        {
            for (int i = worker.EmploymentDate.Year; i < date.Year; i++)
            {
                if (worker.CurrnetBonus_InPercents + worker.Bonus_InPercents < worker.BonusLimit_InPercents)
                {
                     worker.CurrnetBonus_InPercents += worker.Bonus_InPercents;
                }
            }

            return (Decimal)worker.CurrnetBonus_InPercents * worker.Rate_InRuble;
        }

        public decimal Calculate1LineSubbordinatesBonusSalary(Worker worker)
        {
            decimal premium = 0;

            if (worker is IBoss)
            {
                var boss = (IBoss)worker;
                
                foreach (var s in boss.SubWorkers)
                {
                     premium += s.Salary_InRubles * (Decimal)boss.BonusForTheWorkers;
                }
            }

            return premium;
        }
    }
}
