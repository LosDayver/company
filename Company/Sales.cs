﻿using Company.CalculationRules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Company
{
    ///Сотрудник +
    ///Начальник +
    ///Подчиненный +
    ///Вид сотрудника +
    ///Базовая ставка +
    ///Есть % добавки за год работы min, max значения +
    ///Есть % добавки за подчиненных +
    ///Зарплата
    ///Год работы +

    public class Sales : Worker, IYearBonusApplicable, IAllSubworkersBonusApplicable
    {
        public decimal YearBonusInPercents { get; set; }
        public decimal MaxBonusInPercents { get; set; }
        public List<Worker> SubWorkers { get; set; }
        public double BonusForTheWorkers { get; set; }

        public Sales(Factory factory, params Worker[] subWorkers) : base(factory)
        {
            YearBonusInPercents = 3;
            MaxBonusInPercents = 30;
            BonusForTheWorkers = 0.3;
            SubWorkers = subWorkers.ToList();
        }
    }
}
