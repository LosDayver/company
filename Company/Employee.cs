﻿using Company.CalculationRules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Company
{
    ///Сотрудник +
    ///Начальник +
    ///Подчиненный +
    ///Вид сотрудника +
    ///Базовая ставка +
    ///Есть % добавки за год работы min, max значения +
    ///Есть % добавки за подчиненных +
    ///Зарплата
    ///Год работы +

    public class Employee : Worker, IYearBonusApplicable
    {

        public decimal YearBonusInPercents { get; set; }
        public decimal MaxBonusInPercents { get; set; }

        public Employee(Factory factory) : base(factory)
        {
            YearBonusInPercents = 3;
            MaxBonusInPercents = 30;
        }
    }
}
