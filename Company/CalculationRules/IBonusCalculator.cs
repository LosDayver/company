﻿using Company.CalculationRules;
using System;
using System.Collections.Generic;
using System.Text;

namespace Company
{

    public interface IBonusCalculator
    {

    }
    public interface IBonusCalculator<T> : IBonusCalculator
        where T : IBonusApplicable
    {
        decimal CalculateBonus(T worker, object data);
    }
}
