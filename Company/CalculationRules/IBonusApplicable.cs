﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.CalculationRules
{
    public interface IBonusApplicable
    {
        decimal Rate_InRuble { get; set; }
    }

    public interface IYearBonusApplicable : IBonusApplicable
    {
        DateTime EmploymentDate { get; set; }
        decimal YearBonusInPercents { get; set; }
        decimal MaxBonusInPercents { get; set; }
    }

    public interface FirtsLevelSubworkersBonusApplicable : IBonusApplicable
    {
        List<Worker> SubWorkers { get; set; }
        double BonusForTheWorkers { get; set; }
    }
    public interface IAllSubworkersBonusApplicable : IBonusApplicable
    {
        List<Worker> SubWorkers { get; set; }
        double BonusForTheWorkers { get; set; }
    }


}
