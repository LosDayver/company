﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Company
{
    ///Сотрудник +
    ///Начальник +
    ///Подчиненный +
    ///Вид сотрудника +
    ///Базовая ставка +
    ///Есть % добавки за год работы min, max значения +
    ///Есть % добавки за подчиненных +
    ///Зарплата
    ///Год работы +
    
    public interface IBoss
    {
        List<Worker> SubWorkers { get; set; }
        double BonusForTheWorkers { get; set; }
    }
}
