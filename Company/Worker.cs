﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Company
{
    using CalculationRules;

    public abstract class Worker : IBonusApplicable
    {
        public Factory CurrentFactory { get; set; }
        public string Name { get; set; }
        public DateTime EmploymentDate { get; set; }
        public decimal Salary_InRubles { get; set; }
        public decimal Rate_InRuble { get; set; }
        public IBoss Boss { get; set; }
        public List<IBonusCalculator> BonusCalculationRules { get; set; }

        public Worker(Factory currentFactory)
        {
            CurrentFactory = currentFactory;
            Rate_InRuble = currentFactory.Rate;
        }
        public int GetWorkTime()
        {
            return GetWorkTime(DateTime.Now);
        }

        public int GetWorkTime(DateTime dateNow)
        {
            return 5;
        }
    }
}
