﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company
{
    public interface IBookKeeping
    {
        void CalculateSalary(Worker worker);
        void CalculateSalaryForWorkers(List<Worker> workers);
    }
}
