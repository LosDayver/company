﻿using Company.CalculationRules;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Company
{
    public class Manager : Worker, IYearBonusApplicable, FirtsLevelSubworkersBonusApplicable
    {
        public List<Worker> SubWorkers { get; set; }
        public double BonusForTheWorkers { get; set; }
        public decimal YearBonusInPercents { get; set; }
        public decimal MaxBonusInPercents { get; set; }

        public Manager(Factory factory, params Worker[] subWorkers) : base(factory)
        {
            YearBonusInPercents = 5;
            MaxBonusInPercents = 40;
            BonusForTheWorkers = 0.5;
            SubWorkers = subWorkers.ToList();
        }
    }
}
