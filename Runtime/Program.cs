﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Company;
using Company.CalculationRules;

namespace Runtime
{
    class Program
    {
        static void Main(string[] args)
        {
            var factory = new Factory() { Name = "  "};

            //Console.WriteLine("Введите название компании");
            //var factory = new Factory() { Name = Console.ReadLine() };
            //Console.Clear();

            //Console.WriteLine("Введите название компании");
            //factory.Rate = Decimal.Parse(Console.ReadLine());
            //Console.Clear();

            var sales = new Sales(factory);

            sales.BonusCalculationRules.AddRange(new List<IBonusCalculator> { new YearBonusCalculator(), new FirstLevelSubworkersBonusCalculator(), new AllSubworkersBonusCalculator() });


            factory.Workers = new List<Worker> { };
            
            Console.ReadKey();
        }
    }
}
